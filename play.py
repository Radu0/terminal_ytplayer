import os
import sys
import youtube_dl
from playsound import playsound
import requests
import re
from colorama import Fore, Back

# These will probbly be used in future versions
#def blockPrint():
#    sys.stdout = open(os.devnull, 'w')
#
#def enablePrint():
#    sys.stdout = sys.__stdout__

def download_audio(url):
    print(Fore.RED + 'Downloading...' + Fore.LIGHTBLACK_EX)
    
    ydl_opts = {
        'format':'bestaudio/best',
        'outtmpl': 'track.%(ext)s',
        'postprocessors': [{
            'key': 'FFmpegExtractAudio',
            'preferredcodec': 'mp3',
            'preferredquality': '192', }]
    }
    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        ydl.download([url])

def main():
    print(Back.BLACK + 'Insert video name' + Back.RESET + '\n\n')
    skw=str(input())
    #skw='rousseau piano'
    for l in skw:
        if l == ' ':
            l='+'
    
    search='https://www.youtube.com/results?search_query=' + skw
    
    payload={'lang':'en'}
    
    r = requests.get(search, params=payload)
    
    #print(r.text)
    
    #print(skw)
    
    video_ids = re.findall(r"watch\?v=(\S{11})", str(r.text))
    link=(f'https://www.youtube.com/watch?v={video_ids[0]}')
    print(Fore.GREEN + f'Link - {link}\n')
    try:
        os.remove('track.mp3')
    except:
        pass
    #os.system(f'youtube-dl -x --audio-format mp3 --prefer-ffmpeg -o track.mp3 {link}')
    download_audio(link)
    print(Fore.BLUE + 'Playing track\n' + Fore.RESET)
    playsound('track.mp3')

while True:
    main()
    if input(Fore.MAGENTA + "play another? (Y/N) " + Fore.RESET).strip().upper() != 'Y':
        break
