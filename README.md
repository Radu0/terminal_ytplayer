<h1>terminal_ytplayer (WIP)</h1>
<h3>plays any audio from youtube in the terminal</h3>

<h4>Downloading:</h4>
<pre>
    git clone https://gitlab.com/Radu0/terminal_ytplayer
</pre>

<h4>Running</h4>
<pre>
python play.py
</pre>

**Note: you must be in the project directory when you run this**


<h4>Dependencies</h4>
<pre>python, pip(python library/module installer), playsound(pip install playsound), requests(pip install requests), youtube-dl(pip install youtube-dl), colorama(pip install colorama), ffmpeg</pre>
<p>most of these can be installed with a package manager on linux, on windows it's harder but if you are a terminal user you can probably figure it out, I won't do a step by step how to install here</p>


<h4>How it works</h4>
<p>You insert a string, the program searches youtube for the first result, downloads it as an mp3 and plays it. the audio file remains in the folder until you run the program again when it is deleted and replaced</p>
